;; modules/config.fnl
;; Loads and provides configuration values from environment variables

;; -------------------------------------------------------------------------- ;;
;; Dependencies

(local ezenv (require :ezenv))

;; -------------------------------------------------------------------------- ;;
;; Configuration structure

(local config_structure {:auth_secret {:type "string"
                                       :required true}
                         :enable_control_panel {:type "boolean"
                                                :default true}
                         :enable_landing_page {:type "boolean"
                                               :default true}
                         :landing_page_index {:type "string"
                                              :default "index.html"}
                         :listen_port {:type "number"
                                       :default 80}
                         :query_limit {:type "number"
                                       :default 100}
                         :session_max_length_hours {:type "number"
                                                    :default 24}
                         :session_max_idle_length_hours {:type "number"
                                                         :default 1}
                         :use_secure_cookies {:type "boolean"
                                              :default true}
                         :use_totp {:type "boolean"
                                    :default true}})

;; -------------------------------------------------------------------------- ;;
;; Load and provide config

(ezenv.load config_structure)
